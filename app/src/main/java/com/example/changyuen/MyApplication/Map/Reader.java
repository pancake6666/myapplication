package com.example.changyuen.MyApplication.Map;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by changyuen on 2016/9/5.
 */
public class Reader {
    private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";

    public List<MarkerOptions> read(InputStream inputStream) throws JSONException {
        List<MarkerOptions> items = new ArrayList<>();
        String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            String type = object.getString("P_Type");
            double lat = object.getDouble("P_Lat");
            double lng = object.getDouble("P_Lng");
            LatLng latLng = new LatLng(lat, lng);
            items.add(new MarkerOptions().position(latLng).snippet("false"));
        }
        return items;
    }
}
