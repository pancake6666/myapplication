/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.changyuen.MyApplication.Map;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

public class MyItemReader {

    /*
     * This matches only once in whole input,
     * so Scanner.next returns whole InputStream as a String.
     * http://stackoverflow.com/a/5445161/2183804
     */
    private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";

    public MarkerItem read(InputStream inputStream, LatLng latLng) throws JSONException {
        MarkerItem items = new MarkerItem();
        String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            double lat = object.getDouble("lat");
            double lng = object.getDouble("lon");
            if (lat == latLng.latitude && lng == latLng.longitude) {
                items.date = object.getString("TransDate");
                items.price = object.getInt("price");
            }

        }
        return items;
    }
}
//, tPrice, pPrice, BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED), date, area , x, y


//    int tPrice = object.getInt("Tprice");
//    double area = object.getDouble("Landarea");
//    int x = object.getInt("x");
//    int y = object.getInt("y");
//    String date = object.getString("TransDate");
//    int pPrice = (int) ((tPrice / area) * 3.31);