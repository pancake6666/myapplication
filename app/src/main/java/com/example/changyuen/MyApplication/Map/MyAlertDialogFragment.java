package com.example.changyuen.MyApplication.Map;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

public class MyAlertDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        LayoutInflater inflater = LayoutInflater.from(getActivity());
//        final View v = inflater.inflate(R.layout.add_dialog,null);
        return new AlertDialog.Builder(getActivity())
                // set Dialog Title
                .setTitle("警告")
                // Set Dialog Message
                .setMessage("您確定要跳轉嗎？")
                // positive button
                .setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getActivity(), CustomMapActivity.class));
                    }
                })
                // negative button
                .setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create();
    }

}
