package com.example.changyuen.MyApplication.SqlDisplay;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.changyuen.MyApplication.R;
import com.example.changyuen.MyApplication.SqlConnect.House;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener, CompoundButton.OnCheckedChangeListener {

    private List<House> mItems;
    private Listener mListener;


    public CardAdapter(List<House> items, Listener listener) {
        if (items == null) {
            items = new ArrayList<>();
        }
        mItems = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_card_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        House house = mItems.get(i);
        if (house.getAddType() == 1) {
            viewHolder.tvType.setText("◉");
            viewHolder.tvAddress.setText(house.getAddress());
            viewHolder.tvType.setTextColor(Color.BLUE);
        } else {
            viewHolder.tvType.setText("◉");
            viewHolder.tvAddress.setText(house.getAddress());
            viewHolder.tvType.setTextColor(Color.RED);
        }
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(house.getDatetime());
        viewHolder.tvTime.setText(formatter.format(date));


        if (mListener != null) {
            viewHolder.addButton.setTag(house.getId());
            viewHolder.addButton.setOnClickListener(this);

            viewHolder.checkBox.setOnCheckedChangeListener(this);
            viewHolder.checkBox.setTag(house.getId());

            viewHolder.cardView.setOnClickListener(this);
            viewHolder.cardView.setOnLongClickListener(this);
            viewHolder.cardView.setTag(house);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType;
        public TextView tvAddress;
        public TextView tvTime;
        public CardView cardView;
        public Button addButton;
        public CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            tvType = (TextView) itemView.findViewById(R.id.type);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            tvAddress = (TextView) itemView.findViewById(R.id.address);
            addButton = (Button) itemView.findViewById(R.id.addcompare);
            tvTime = (TextView) itemView.findViewById(R.id.addtime);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_view:
                if (v instanceof CardView) {
                    House house = (House) v.getTag();
                    mListener.onItemClicked(house);
                }
                break;
            case R.id.addcompare:
                if (v instanceof Button) {
                    Long id = (Long) v.getTag();
                    mListener.onButtonClicked(id);
                }
                break;
        }
    }


    @Override
    public boolean onLongClick(View view) {
        if (view instanceof CardView) {
            House house = (House) view.getTag();
            mListener.onItemLongClicked(house);
        }
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton instanceof CheckBox) {
            Long id = (Long) compoundButton.getTag();
            mListener.onChecked(id, b);
        }
    }


    public List<House> getItems() {
        return mItems;
    }

    public interface Listener {
        void onItemClicked(House house);

        void onChecked(Long id, boolean b);

        void onItemLongClicked(House house);

        void onButtonClicked(Long id);

    }


}
