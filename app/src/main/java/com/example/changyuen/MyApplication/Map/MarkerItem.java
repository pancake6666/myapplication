package com.example.changyuen.MyApplication.Map;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MarkerItem implements ClusterItem {
    public int total;
    public int price;
    public BitmapDescriptor bitmap;
    public String date;
    private LatLng latLng;
    public Double space;
    public int x;
    public int y;
    public boolean hide;

    //, int total, int price, BitmapDescriptor bitmapColor, String date, Double space, int x, int y
    public MarkerItem(double lat, double lng, boolean hide, int price, BitmapDescriptor bitmapColor) {
//        this.total = total;
        this.price = price;
        this.bitmap = bitmapColor;
//        this.date = date;
        this.hide = hide;
        latLng = new LatLng(lat, lng);
//        this.space = space;
//        this.x = x;
//        this.y = y;
    }

    public MarkerItem(int y, int x, Double space, String date, int price, int total) {
        this.y = y;
        this.x = x;
        this.space = space;
        this.date = date;
        this.price = price;
        this.total = total;
    }

    public MarkerItem() {
    }

    @Override
    public LatLng getPosition() {
        return this.latLng;
    }
}
