package com.example.changyuen.MyApplication.Map;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.changyuen.MyApplication.R;
import com.example.changyuen.MyApplication.SqlConnect.House;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;

import java.io.InputStream;


public class HouseInfoDialog extends DialogFragment {

    public String title;
    public String address;
    public Double lng;
    public Double lat;
    public int price;
    public Double space;
    public House house;
    private ProgressDialog mProgressDialog;
    public MarkerItem items;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        final HouseDAO houseDAO = new HouseDAO(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.add_dialog, null);
        AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("房屋資訊")
                .setPositiveButton("加入最愛", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //houseDAO.insert(house);
                        Toast.makeText(getActivity(), "已加入", Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                })
                .setNegativeButton("關閉", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                })
                .create();
        try {
            InputStream inputStream = getResources().openRawResource(R.raw.estate);
            items = new MyItemReader().read(inputStream, new LatLng(lat, lng));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        View locationV = v.findViewById(R.id.location);
        ((TextView) locationV).setText(items.date);
        TextView priceV = (TextView) v.findViewById(R.id.priceX);
        priceV.setText(String.valueOf(items.price));
        return alertDialog;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSpace(Double space) {
        this.space = space;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
//        System.out.println(new Date().getTime());
//        house = new House(0, 0, address, new Date().getTime(), lat, lng, price / 10000, space);
}
