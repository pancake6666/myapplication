package com.example.changyuen.MyApplication.SqlDisplay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.changyuen.MyApplication.R;
import com.example.changyuen.MyApplication.SqlConnect.House;
import com.example.changyuen.MyApplication.SqlConnect.HouseDAO;

import java.util.ArrayList;

public class TableFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    HouseDAO houseDAO;


    public static TableFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        TableFragment fragment = new TableFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        houseDAO = new HouseDAO(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_table, container, false);
        TableLayout tableLayout = (TableLayout) view.findViewById(R.id.score_table);
        House one = houseDAO.get((long) integers.get(0));
        House two = houseDAO.get((long) integers.get(1));
        for (int i = 0; i < houseDAO.getColumnCount(); i++) {
            switch (i) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:

            }
            TableRow tableRow = new TableRow(getContext());
            TextView column = new TextView(getContext());
            TextView house1 = new TextView(getContext());
            house1.setText(one.getAddress());
            TextView house2 = new TextView(getContext());
            house2.setText(two.getAddress());
            tableRow.addView(column, 0);
            tableRow.addView(house1, 1);
            tableRow.addView(house2, 2);
            tableLayout.addView(tableRow);
        }

        return view;
    }
}
