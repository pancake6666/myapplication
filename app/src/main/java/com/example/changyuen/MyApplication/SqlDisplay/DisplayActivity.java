package com.example.changyuen.MyApplication.SqlDisplay;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.changyuen.MyApplication.MainActivity;
import com.example.changyuen.MyApplication.R;
import com.example.changyuen.MyApplication.SqlConnect.House;
import com.example.changyuen.MyApplication.SqlConnect.HouseDAO;
import com.google.common.collect.EvictingQueue;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;


public class DisplayActivity extends AppCompatActivity implements CardAdapter.Listener, AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnLongClickListener {
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    FloatingActionButton floatingActionButton;
    CardAdapter mAdapter;
    List<House> houses;
    HouseDAO houseDAO;
    Button compareb;
    BadgeView badgeView;
    TextView badge;
    ArrayList<Long> removelist = new ArrayList<>();
    ArrayAdapter<String> spinnerlist;
    public Queue<Long> compare = EvictingQueue.create(2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setVisibility(View.INVISIBLE);

        assert mRecyclerView != null;
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CardAdapter(null, this);
        mAdapter.notifyDataSetChanged();

        houseDAO = new HouseDAO(getApplicationContext());

        compareb = (Button) findViewById(R.id.compare);
        compareb.setOnClickListener(this);
        compareb.setOnLongClickListener(this);
        badge = (TextView) findViewById(R.id.badge_notification_2);


        String[] list = {"不限", "單價高至低", "加入日期新至舊", "實價登入優先", "定位加入優先"};
        spinnerlist = new ArrayAdapter<>(DisplayActivity.this, R.layout.spinner_item, list);
        spinner.setAdapter(spinnerlist);
        spinner.setOnItemSelectedListener(this);


    }


    public void setOnAdpter(List<House> houses) {
        mAdapter.getItems().clear();
        mAdapter.getItems().addAll(houses);
        mRecyclerView.setAdapter(mAdapter);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!removelist.isEmpty()) {
                    for (Long id : removelist) {
                        houseDAO.delete(id);
                    }
                    removelist.clear();
                    startActivity(new Intent(DisplayActivity.this, DisplayActivity.class));
                } else {
                    Toast.makeText(getApplicationContext(), "無勾選", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public void onItemClicked(House house) {
        if (house != null) {
            DisplayDialog displayDialog = new DisplayDialog();
            displayDialog.setAddress(house.getAddress());
            displayDialog.setPrice(house.getPrice());
            displayDialog.setSpace(house.getSpace());
            displayDialog.show(getFragmentManager(), "displayDialog");
        }
    }

    @Override
    public void onChecked(Long id, boolean b) {
        if (id != null) {
            if (b) {
                removelist.add(id);
                floatingActionButton.setVisibility(View.VISIBLE);
            } else {
                removelist.remove(id);
                if (removelist.isEmpty()) {
                    floatingActionButton.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    @Override
    public void onItemLongClicked(House house) {
        if (house != null && house.getAddType() == 1) {
            EditDialog editDialog = new EditDialog();
            editDialog.show(getFragmentManager(), "editDialog");
        }
    }

    @Override
    public void onButtonClicked(Long id) {
        if (id != null) {
            if (compare.contains(id)) {
                Toast.makeText(getApplication(), "已在列表中", Toast.LENGTH_SHORT).show();
            } else if (compare.size() < 2) {
                compare.offer(id);
                badge.setText(String.valueOf(compare.size()));
                Toast.makeText(getApplication(), "已加入", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplication(), "未加入，最多比較兩筆", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 0) {
            houses = houseDAO.getAll();
            mAdapter.notifyDataSetChanged();
            setOnAdpter(houses);
        } else if (i == 1) {
            houses = houseDAO.getOrder("price", "DESC");
            mAdapter.notifyDataSetChanged();
            setOnAdpter(houses);
        } else if (i == 2) {
            houses = houseDAO.getOrder("addtime", "DESC");
            mAdapter.notifyDataSetChanged();
            setOnAdpter(houses);
        } else if (i == 3) {
            houses = houseDAO.getOrder("addtype", "ASC");
            setOnAdpter(houses);
        } else if (i == 4) {
            houses = houseDAO.getOrder("addtype", "DESC");
            setOnAdpter(houses);
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        if (!badge.getText().equals("2") || badge.getText() == null) {
            System.out.println("比較列無物件");
        } else {
            System.out.println("比較～");
            Bundle bundle = new Bundle();
            ArrayList<Integer> comparelist = new ArrayList<>();
            for (long id : compare) {
                comparelist.add((int) id);
            }
            bundle.putIntegerArrayList("id", comparelist);
            Intent i = new Intent(this, CompareActivity.class);
            i.putExtras(bundle);
            startActivity(i);

        }
    }

    @Override
    public boolean onLongClick(View view) {
        compare.clear();
        badge.setText(String.valueOf(compare.size()));
        Toast.makeText(getApplication(), "已清空", Toast.LENGTH_SHORT).show();
        return false;
    }
}
