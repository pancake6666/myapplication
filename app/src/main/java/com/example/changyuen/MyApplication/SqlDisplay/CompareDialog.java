package com.example.changyuen.MyApplication.SqlDisplay;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class CompareDialog extends DialogFragment {

    public boolean agree = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setTitle("警告").setMessage("一次只能比較兩個物件。\n選擇仍然加入會將第一個物件刪除。")
                .setPositiveButton("仍然加入", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        agree = true;
                        dismiss();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                })
                .create();
    }
}
