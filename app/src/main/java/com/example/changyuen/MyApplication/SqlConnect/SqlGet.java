package com.example.changyuen.MyApplication.SqlConnect;

import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlGet {
    public static Connection conn;
    public String state = null;
    Statement statement;
    private boolean isOpen = false;

    public boolean isOpen() {
        return isOpen;
    }

    private Connection getConn() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection = null;
        String ConnectionURL = null;
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
            ConnectionURL = "jdbc:jtds:sqlserver://140.136.156.36:1433;DatabaseName=RealEstate_final;charset=utf8;user=sa;password=Root1234";
            connection = DriverManager.getConnection(ConnectionURL);
        } catch (SQLException se) {
            Log.e("ERRO", se.getMessage());
            System.out.println(se.toString());
        } catch (ClassNotFoundException e) {
            Log.e("ERRO", e.getMessage());
            System.out.println(e.toString());
        } catch (Exception e) {
            Log.e("ERRO", e.getMessage());
            System.out.println(e.toString());
        }
        return connection;
    }

    public void sqlsetting() {
        try {
            conn = getConn();
            if (conn.isClosed() == false) {
                isOpen = true;
                state = "connect ok";
            } else {
                isOpen = false;
                state = "connect fail";
            }

        } catch (Exception ex) {

            ex.printStackTrace();

        }

    }


}

