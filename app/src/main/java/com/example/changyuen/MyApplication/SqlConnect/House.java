package com.example.changyuen.MyApplication.SqlConnect;


public class House {
    private long id;
    private int addType; //實價登錄 = 0;新物件 = 1;
    private String address;


    private long datetime;
    private double lat;
    private double lng;
    private int price;
    private double space;

    public House() {
    }

    public House(long id, int addType, String address, long datetime, double lat, double lng, int price, double space) {
        this.id = id;
        this.addType = addType;
        this.address = address;
        this.datetime = datetime;
        this.lat = lat;
        this.lng = lng;
        this.price = price;
        this.space = space;
    }

    public int getAddType() {
        return addType;
    }

    public void setAddType(int addType) {
        this.addType = addType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getSpace() {
        return space;
    }

    public void setSpace(double space) {
        this.space = space;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }
}
