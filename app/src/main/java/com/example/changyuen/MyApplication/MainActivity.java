package com.example.changyuen.MyApplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.changyuen.MyApplication.Map.ClusteringDemoActivity;
import com.example.changyuen.MyApplication.Map.MapsActivity;
import com.example.changyuen.MyApplication.SqlConnect.House;
import com.example.changyuen.MyApplication.SqlConnect.HouseDAO;
import com.example.changyuen.MyApplication.SqlConnect.User;
import com.example.changyuen.MyApplication.SqlConnect.UserDAO;
import com.example.changyuen.MyApplication.SqlDisplay.DisplayActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewGroup mListView;
    private TextView textView;
    UserDAO userDAO;
    HouseDAO houseDAO;
    List<User> users;
    List<House> houses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/NotoSansCJKtc-Regular.otf");
        mListView = (ViewGroup) findViewById(R.id.list);
        addfunction("開始使用", MapsActivity.class);
        addfunction("物件", DisplayActivity.class);
        addfunction("google", ClusteringDemoActivity.class);

//        SqlGet sqlGet = new SqlGet();
//        sqlGet.sqlsetting();
//        textView = new TextView(this);
//        textView.setText(sqlGet.state);
//        mListView.addView(textView);

        userDAO = new UserDAO(getApplicationContext());
        houseDAO = new HouseDAO(getApplicationContext());

        if (userDAO.getCount() == 0) {
            userDAO.sample();
        }

        users = userDAO.getAll();
        for (User user : users) {
            TextView textView1 = new TextView(this);
            textView1.setText(user.getId() + " " + user.getName());
            mListView.addView(textView1);
        }

    }

    private void addfunction(String funcnName, Class<? extends Activity> activityClass) {
        Button button = new Button(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        button.setLayoutParams(layoutParams);
        button.setText(funcnName);
        button.setTag(activityClass);
        button.setOnClickListener(this);
        mListView.addView(button);
    }

    @Override
    public void onClick(View v) {
        Class activityClass = (Class) v.getTag();
        startActivity(new Intent(this, activityClass));
    }

}
