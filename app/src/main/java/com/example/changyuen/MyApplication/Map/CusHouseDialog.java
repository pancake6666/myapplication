package com.example.changyuen.MyApplication.Map;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.changyuen.MyApplication.R;
import com.example.changyuen.MyApplication.SqlConnect.House;
import com.example.changyuen.MyApplication.SqlConnect.HouseDAO;

import java.util.Date;


public class CusHouseDialog extends DialogFragment {
    public int price;
    public Double lng;
    public Double lat;
    public String address;
    public Double space;
    public House house;
    public boolean check = false;

    int price_Input;
    String address_Input;
    double space_Input;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final HouseDAO houseDAO = new HouseDAO(getActivity());

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.cushousedialog, null);

        final EditText addressE = (EditText) v.findViewById(R.id.addressinput);
        addressE.setText(address);

        final EditText priceE = (EditText) v.findViewById(R.id.priceinput);
        priceE.setHint("單位：萬元");

        final EditText spaceE = (EditText) v.findViewById(R.id.spaceinput);

        return new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("房屋資訊輸入")
                .setPositiveButton("送出並分析", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        address_Input = addressE.getText().toString();
                        if (TextUtils.isEmpty(address_Input)) {
                            addressE.setError("必填項目");
                            check = false;
                        }

                        if (TextUtils.isEmpty(priceE.getText().toString())) {
                            priceE.setError("必填項目");
                            check = false;
                        } else {
                            price_Input = Integer.parseInt(priceE.getText().toString());
                            check = true;
                        }

                        if (TextUtils.isEmpty(spaceE.getText().toString())) {
                            spaceE.setError("必填項目");
                            check = false;
                        } else {
                            space_Input = Double.parseDouble(spaceE.getText().toString());
                            check = true;
                        }
                        if (check) {
                            System.out.println(new Date().getTime());
                            house = new House(0, 1, address_Input, new Date().getTime(), lat, lng, price_Input, space_Input);
                            System.out.println(house.getPrice());
                            houseDAO.insert(house);
                            dismiss();
                        }
                    }
                })
                .setNegativeButton("關閉", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                })
                .create();
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setSpace(Double space) {
        this.space = space;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
