package com.example.changyuen.MyApplication.SqlDisplay;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.changyuen.MyApplication.R;

public class DisplayDialog extends DialogFragment {

    public String title;
    public String address;
    public Double lng;
    public Double lat;
    public int price;
    public Double space;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.displaydetail, null);

        View locationV = v.findViewById(R.id.location);
        ((TextView) locationV).setText(address);
        View priceV = v.findViewById(R.id.price);
        ((TextView) priceV).setText(String.valueOf(price));
        View lovebtn = v.findViewById(R.id.love);
        lovebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("房屋資訊")
                .create();
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSpace(Double space) {
        this.space = space;
    }
}
