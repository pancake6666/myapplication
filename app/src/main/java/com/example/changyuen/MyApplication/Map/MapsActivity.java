package com.example.changyuen.MyApplication.Map;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.util.LruCache;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.SeekBar;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.example.changyuen.MyApplication.MainActivity;
import com.example.changyuen.MyApplication.R;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MapsActivity extends BaseMapActivity implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<MarkerItem>, ClusterManager.OnClusterInfoWindowClickListener<MarkerItem>, ClusterManager.OnClusterItemClickListener<MarkerItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MarkerItem> {

    private FloatingActionMenu menu_disaster;
    private FloatingActionMenu menu_dislike;
    private ClusterManager<MarkerItem> clusterManager;
    private GeoJsonLayer activeLayer;
    private GeoJsonLayer floodLayer;
    private GeoJsonLayer soilLayer;
    private GeoJsonLayer pointLayer;
    private ProgressDialog psDialog;
    private List<FloatingActionMenu> menus = new ArrayList<>();
    private List<MarkerItem> realitems = new ArrayList<>();
    private Handler mUiHandler = new Handler();
    private FloatingSearchView mSearchView;
    private IconGenerator iconGenerator;
    private List<MarkerOptions> items;
    private LruCache<String, Bitmap> mMemoryCache;
    private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";
    private ProgressDialog mProgressDialog;
    private FloatingActionButton fab_active;
    private FloatingActionButton fab_flood;
    private FloatingActionButton fab_soil;

    @Override
    protected void startDemo() {
        //地圖定位設定
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.047908, 121.517315), 10));
        getMap().getUiSettings().setMapToolbarEnabled(false);

        //資料載入
        psDialog = ProgressDialog.show(this, "提示", "資料載入中，請稍候...");
        clusterManager = new ClusterManager<MarkerItem>(this, getMap());
        LoadJsonTask loadJsonTask = new LoadJsonTask();
        InputStream inputStream = getResources().openRawResource(R.raw.estate);
        loadJsonTask.execute(inputStream);
        getMap().setOnCameraChangeListener(clusterManager);


        //元件
        menu_disaster = (FloatingActionMenu) findViewById(R.id.menu_disaster);
        menu_dislike = (FloatingActionMenu) findViewById(R.id.menu_deslike);

        fab_active = (FloatingActionButton) findViewById(R.id.fab_activefault);
        fab_flood = (FloatingActionButton) findViewById(R.id.fab_flood);
        fab_soil = (FloatingActionButton) findViewById(R.id.fab_soil);

        fab_active.setOnClickListener(disasterClickListener);
        fab_flood.setOnClickListener(disasterClickListener);
        fab_soil.setOnClickListener(disasterClickListener);

        FloatingActionButton fab_incinerator = (FloatingActionButton) findViewById(R.id.fab_incinerator);
        FloatingActionButton fab_mortuary = (FloatingActionButton) findViewById(R.id.fab_mortuary);
        FloatingActionButton fab_gas = (FloatingActionButton) findViewById(R.id.fab_gas);

        fab_incinerator.setOnClickListener(pointClickListener);

        menu_disaster.hideMenuButton(false);
        menu_dislike.hideMenuButton(false);
        menu_disaster.setClosedOnTouchOutside(true);
        menu_dislike.setClosedOnTouchOutside(true);
        menus.add(menu_disaster);
        menus.add(menu_dislike);
        int delay = 400;
        for (final FloatingActionMenu menu : menus) {
            mUiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu.showMenuButton(true);
                }
            }, delay);
            delay += 150;
        }
        menu_disaster.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menu_disaster.isOpened()) {
                    menu_disaster.close(false);
                } else {
                    if (menu_dislike.isOpened()) {
                        menu_dislike.close(false);
                        menu_disaster.open(false);
                    } else {
                        menu_disaster.open(false);
                    }
                }
            }
        });
        menu_dislike.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menu_dislike.isOpened()) {
                    menu_dislike.close(false);
                } else {
                    if (menu_disaster.isOpened()) {
                        menu_disaster.close(false);
                        menu_dislike.open(false);
                    } else {
                        menu_dislike.open(false);
                    }
                }
            }
        });
        createCustomAnimation();
        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        setupFloatingSearch();
        seekbar();


        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };


    }

    //LruCache
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            bitmap.setHasAlpha(false);
            bitmap.setConfig(Bitmap.Config.RGB_565);
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    //cluster載入
    private void clusterLoad(int range) {
        getMap().clear();
        clusterManager.setRenderer(new markerRender(range));
        getMap().setOnCameraChangeListener(clusterManager);
        getMap().setOnMarkerClickListener(clusterManager);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.cluster();
    }


    @Override
    public boolean onClusterClick(Cluster<MarkerItem> cluster) {
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MarkerItem> cluster) {

    }

    //marker觸發
    @Override
    public boolean onClusterItemClick(MarkerItem markerItem) {
        showProgressDialog();
        HouseInfoDialog houseInfoDialog = new HouseInfoDialog();
        houseInfoDialog.setLat(markerItem.getPosition().latitude);
        houseInfoDialog.setLng(markerItem.getPosition().longitude);
//        houseInfoDialog.setPrice(markerItem.price);
//        houseInfoDialog.setAddress(markerItem.date);
//        houseInfoDialog.setSpace(markerItem.space);
        houseInfoDialog.show(getFragmentManager(), "houseInfoDialog");
        hideProgressDialog();
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MarkerItem markerItem) {
    }

    @Override
    protected void onDestroy() {
        psDialog.dismiss();
        super.onDestroy();
    }

    //FloatingActionMenu 動畫
    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(menu_disaster.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(menu_disaster.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(menu_disaster.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(menu_disaster.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(150);
        scaleOutY.setDuration(150);

        scaleInX.setDuration(50);
        scaleInY.setDuration(50);

        scaleOutX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                menu_disaster.getMenuIconView().setImageResource(menu_disaster.isOpened()
                        ? R.drawable.ic_edit : R.drawable.ic_close);
            }
        });

        set.play(scaleInX).with(scaleInY);
        set.play(scaleOutX).with(scaleOutY).after(scaleInX);
        set.setInterpolator(new OvershootInterpolator(2));

        menu_disaster.setIconToggleAnimatorSet(set);
    }

    //FloatingSearchView setup
    private void setupFloatingSearch() {
        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.action_search) {
                    onMapSearch(mSearchView.getQuery());
                } else if (item.getItemId() == R.id.action_mode) {
                    startActivity(new Intent(MapsActivity.this, CustomMapActivity.class));
                } else if (item.getItemId() == R.id.action_home) {
                    startActivity(new Intent(MapsActivity.this, MainActivity.class));
                }
            }
        });
        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                List<Address> addressList = null;

                if (currentQuery != null || !currentQuery.equals("")) {
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        addressList = geocoder.getFromLocationName(currentQuery, 3);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addressList.isEmpty() == false) {
                        Address address = addressList.get(0);
                        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        getMap().animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    }
                }
            }
        });
    }

    //地址搜尋
    public void onMapSearch(String location) {
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 3);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressList.isEmpty() == false) {
                Address address = addressList.get(0);
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        }
    }


    //cluster marker 設定
    private class markerRender extends DefaultClusterRenderer<MarkerItem> {

        public int getRange() {
            return range;
        }

        public void setRange(int range) {
            this.range = range;
        }

        public int range;

        public markerRender(int range) {
            super(getApplicationContext(), getMap(), clusterManager);
            this.range = range;
        }

        @Override
        protected void onBeforeClusterItemRendered(MarkerItem markerItem, MarkerOptions markerOptions) {
            markerOptions.icon(markerItem.bitmap);
            if (markerItem.price < range) {
                markerOptions.visible(true);
            } else {
                markerOptions.visible(markerItem.hide);
            }
        }

    }

    //非同步讀入json
    private class LoadJsonTask extends AsyncTask<InputStream, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(InputStream... params) {
            long startTime = System.currentTimeMillis();
            InputStream inputStream = params[0];
            //iconGenerator = new IconGenerator(getApplicationContext());
            try {
//                FeatureCollection featureCollection = new FeatureCollection(GeoJSON.parse(inputStream).toJSON());
//                for (int i = 0; i < featureCollection.getFeatures().size(); i++) {
//                    Feature feature = featureCollection.getFeatures().get(i);
//                    if (feature.getProperties().getString("縣市").equals("臺北市") || feature.getProperties().getString("縣市").equals("新北市")) {
//                        Geometry geometry = feature.getGeometry();
//                        JSONArray jsonArray = geometry.toJSON().optJSONArray("coordinates");
//                        LatLng latLng = new LatLng(Double.parseDouble(String.valueOf(jsonArray.get(1))), Double.parseDouble(String.valueOf(jsonArray.get(0))));
//                        String pricestr = feature.getProperties().getString("單價每平方公尺");
//                        String date = feature.getProperties().getString("土地區段位置或建物區門牌");
//                        Double space = feature.getProperties().getDouble("建物移轉總面積平方公尺");
//                        MarkerOptions markerOptions;
//                        if (!feature.getProperties().getString("單價每平方公尺").equals("null")) {
//                            int price = (int) (Integer.parseInt(feature.getProperties().getString("單價每平方公尺")) * 3.31);
//                            if (price < 400000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_WHITE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 400000 && price < 500000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_BLUE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 500000 && price < 600000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 600000 && price < 700000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_GREEN);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else {
//                                iconGenerator.setStyle(IconGenerator.STYLE_RED);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            }
//                            realitems.add(new MarkerItem(latLng.latitude, latLng.longitude, markerOptions.getTitle(), price, markerOptions.getIcon(), date, space, false));
//                        } else {
//                            int price = (int) ((int) ((Integer.parseInt(feature.getProperties().getString("總價元"))) / (Double.parseDouble(feature.getProperties().getString("建物移轉總面積平方公尺")))) * 3.31);
//                            if (price < 400000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_WHITE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 400000 && price < 500000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_BLUE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 500000 && price < 600000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 600000 && price < 700000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_GREEN);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else {
//                                iconGenerator.setStyle(IconGenerator.STYLE_RED);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            }
//                            realitems.add(new MarkerItem(latLng.latitude, latLng.longitude, markerOptions.getTitle(), price, markerOptions.getIcon(), date, space, false));
//                        }
//                    }
//                }
//                for (MarkerItem markerItem : realitems) {
//                    clusterManager.addItem(markerItem);
//                }
//

                String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
                JSONArray array = new JSONArray(json);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    double lat = object.getDouble("lat");
                    double lng = object.getDouble("lon");
                    LatLng latLng = new LatLng(lat, lng);
                    int tPrice = object.getInt("price");
                    float color = object.getInt("color");
                    realitems.add(new MarkerItem(latLng.latitude, latLng.longitude, false, tPrice, BitmapDescriptorFactory.defaultMarker(color)));
                }
                clusterManager.addItems(realitems);
                //InputStream inputPoint = getResources().openRawResource(R.raw.point1);
                //items = new Reader().read(inputPoint);
                activeLayer = new GeoJsonLayer(getMap(), R.raw.activefault, getApplicationContext());
                activeLayer.getDefaultLineStringStyle().setColor(Color.argb(255, 255, 0, 0));
                floodLayer = new GeoJsonLayer(getMap(), R.raw.flooding, getApplicationContext());
                soilLayer = new GeoJsonLayer(getMap(), R.raw.liquefaction, getApplicationContext());
                soilLayer.getDefaultPolygonStyle().setStrokeWidth(0);
                soilLayer.getDefaultPolygonStyle().setFillColor(Color.argb(150, 200, 0, 0));
                pointLayer = new GeoJsonLayer(getMap(), R.raw.point, getApplicationContext());
                long endTime = System.currentTimeMillis();
                long totTime = endTime - startTime;
                System.out.println("Using Time:" + totTime);
                return true;
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            clusterLoad(10000000);
            psDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

    }

    public void seekbar() {
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setMax(40);
        seekBar.setProgress(40);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int newProgress = seekBar.getProgress();
                System.out.println(newProgress);
                if (newProgress == 40) {
                    //seekBar.setProgress(40);
                    clusterLoad(10000000);
                } else if (newProgress >= 30 && newProgress < 40) {
                    //seekBar.setProgress(30);
                    clusterLoad(700000);
                } else if (newProgress >= 20 && newProgress < 30) {
                    //seekBar.setProgress(20);
                    clusterLoad(600000);
                } else if (newProgress >= 10 && newProgress < 20) {
                    //seekBar.setProgress(10);
                    clusterLoad(500000);
                } else {
                    //seekBar.setProgress(0);
                    clusterLoad(400000);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }


    private View.OnClickListener disasterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab_activefault:
                    if (activeLayer.isLayerOnMap()) {
                        activeLayer.removeLayerFromMap();
                        Toast.makeText(getApplicationContext(), "關閉斷層", Toast.LENGTH_SHORT).show();
                        fab_flood.setEnabled(true);
                        fab_soil.setEnabled(true);
                    } else {
                        activeLayer.addLayerToMap();
                        Toast.makeText(getApplicationContext(), "開啟斷層", Toast.LENGTH_SHORT).show();
                        fab_flood.setEnabled(false);
                        fab_soil.setEnabled(false);
                    }
                    menu_disaster.close(true);
                    break;
                case R.id.fab_flood:
                    if (floodLayer.isLayerOnMap()) {
                        floodLayer.removeLayerFromMap();
                        Toast.makeText(getApplicationContext(), "關閉淹水", Toast.LENGTH_SHORT).show();
                        fab_active.setEnabled(true);
                        fab_soil.setEnabled(true);
                    } else {
                        floodLayer.addLayerToMap();
                        Toast.makeText(getApplicationContext(), "開啟淹水", Toast.LENGTH_SHORT).show();
                        fab_active.setEnabled(false);
                        fab_soil.setEnabled(false);
                    }
                    menu_disaster.close(true);
                    break;
                case R.id.fab_soil:
                    if (soilLayer.isLayerOnMap()) {
                        soilLayer.removeLayerFromMap();
                        Toast.makeText(getApplicationContext(), "關閉土壤液化", Toast.LENGTH_SHORT).show();
                        fab_active.setEnabled(true);
                        fab_flood.setEnabled(true);
                    } else {
                        soilLayer.addLayerToMap();
                        Toast.makeText(getApplicationContext(), "開啟土壤液化", Toast.LENGTH_SHORT).show();
                        fab_active.setEnabled(false);
                        fab_flood.setEnabled(false);
                    }
                    menu_disaster.close(true);
                    break;
            }
        }
    };
    private View.OnClickListener pointClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab_incinerator:
                    if (pointLayer.isLayerOnMap()) {
                        pointLayer.removeLayerFromMap();
                    } else {
                        pointLayer.addLayerToMap();
                    }
                    menu_disaster.close(true);
                    break;
            }
        }
    };

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

}


