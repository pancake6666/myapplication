package com.example.changyuen.MyApplication.SqlConnect;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by changyuen on 16/7/26.
 */
public class HouseDAO {
    // 表格名稱
    public static final String TABLE_NAME = "house";

    // 編號表格欄位名稱，固定不變
    public static final String KEY_ID = "_id";

    // 其它表格欄位名稱
    public static final String ADDTYPE_COLUMN = "addtype";
    public static final String ADDRESS_COLUMN = "date";
    public static final String DATETIME_COLUMN = "addtime";
    public static final String LAT_COLUMN = "lat";
    public static final String LNG_COLUMN = "lng";
    public static final String PIRCE_COLUMN = "price";
    public static final String SPACE_COLUMN = "space";

    // 使用上面宣告的變數建立表格的SQL指令
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ADDTYPE_COLUMN + " INTEGER NOT NULL, " +
                    ADDRESS_COLUMN + " TEXT NOT NULL unique, " +
                    DATETIME_COLUMN + " INTEGER NOT NULL, " +
                    LAT_COLUMN + " REAL NOT NULL unique, " +
                    LNG_COLUMN + " REAL NOT NULL unique, " +
                    PIRCE_COLUMN + " INTEGER NOT NULL, " +
                    SPACE_COLUMN + " REAL NOT NULL)";

    // 資料庫物件
    private SQLiteDatabase db;

    // 建構子，一般的應用都不需要修改
    public HouseDAO(Context context) {
        db = SqLite.getDatabase(context);
    }

    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        db.close();
    }

    // 新增參數指定的物件
    public House insert(House house) {
        // 建立準備新增資料的ContentValues物件
        ContentValues cv = new ContentValues();

        // 加入ContentValues物件包裝的新增資料
        // 第一個參數是欄位名稱， 第二個參數是欄位的資料
        cv.put(ADDTYPE_COLUMN, house.getAddType());
        cv.put(ADDRESS_COLUMN, house.getAddress());
        cv.put(DATETIME_COLUMN, house.getDatetime());
        cv.put(LAT_COLUMN, house.getLat());
        cv.put(LNG_COLUMN, house.getLng());
        cv.put(PIRCE_COLUMN, house.getPrice());
        cv.put(SPACE_COLUMN, house.getSpace());

        System.out.println(cv.toString());


        // 新增一筆資料並取得編號
        // 第一個參數是表格名稱
        // 第二個參數是沒有指定欄位值的預設值
        // 第三個參數是包裝新增資料的ContentValues物件
        long id = db.insert(TABLE_NAME, null, cv);

        // 設定編號
        house.setId(id);
        // 回傳結果
        return house;
    }

    // 修改參數指定的物件
    public boolean update(House house) {
        // 建立準備修改資料的ContentValues物件
        ContentValues cv = new ContentValues();

        // 加入ContentValues物件包裝的修改資料
        // 第一個參數是欄位名稱， 第二個參數是欄位的資料
        cv.put(ADDRESS_COLUMN, house.getAddress());
        cv.put(ADDTYPE_COLUMN, house.getAddType());
        cv.put(DATETIME_COLUMN, house.getDatetime());
        cv.put(LAT_COLUMN, house.getLat());
        cv.put(LNG_COLUMN, house.getLng());
        cv.put(PIRCE_COLUMN, house.getPrice());
        cv.put(SPACE_COLUMN, house.getSpace());

        // 設定修改資料的條件為編號
        // 格式為「欄位名稱＝資料」
        String where = KEY_ID + "=" + house.getId();

        // 執行修改資料並回傳修改的資料數量是否成功
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    // 刪除參數指定編號的資料
    public boolean delete(long id) {
        // 設定條件為編號，格式為「欄位名稱=資料」
        String where = KEY_ID + "=" + id;
        // 刪除指定編號資料並回傳刪除是否成功
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    // 讀取所有記事資料
    public List<House> getAll() {
        List<House> result = new ArrayList<>();
        Cursor cursor = db.query(
                TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    // 取得指定編號的資料物件
    public House get(long id) {
        // 準備回傳結果用的物件
        House house = null;
        // 使用編號為查詢條件
        String where = KEY_ID + "=" + id;
        // 執行查詢
        Cursor result = db.query(
                TABLE_NAME, null, where, null, null, null, null, null);

        // 如果有查詢結果
        if (result.moveToFirst()) {
            // 讀取包裝一筆資料的物件
            house = getRecord(result);
        }

        // 關閉Cursor物件
        result.close();
        // 回傳結果
        return house;
    }

    // 把Cursor目前的資料包裝為物件
    public House getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        House result = new House();

        result.setId(cursor.getLong(0));
        result.setAddType(cursor.getInt(1));
        result.setAddress(cursor.getString(2));
        result.setDatetime(cursor.getLong(3));
        result.setLat(cursor.getDouble(4));
        result.setLng(cursor.getDouble(5));
        result.setPrice(cursor.getInt(6));
        result.setSpace(cursor.getDouble(7));

        // 回傳結果
        return result;
    }

    // 取得資料數量
    public int getCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }

        return result;
    }

    public List<House> getOrder(String order, String by) {
        List<House> result = new ArrayList<>();
        Cursor cursor =
                db.rawQuery("SELECT  * FROM " + TABLE_NAME + " ORDER BY " + order + " " + by, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    public int getColumnCount() {
        return 7;
    }


    // 建立範例資料
}
